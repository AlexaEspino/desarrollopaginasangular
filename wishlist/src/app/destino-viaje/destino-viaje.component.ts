import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { Destinos } from '../models/destinos-viaje.model';

@Component({
  selector: 'app-destinos',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinosComponent implements OnInit {
  @Input() destinos: Destinos;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output()clicked: EventEmitter<Destinos>;

  constructor() {
    this.clicked = new EventEmitter();
  }
 
  ngOnInit(): void {
  }

  ir(){
    this.clicked.emit(this.destinos);
    return false;
  }

}
