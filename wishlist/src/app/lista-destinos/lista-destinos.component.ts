import { Component, OnInit } from '@angular/core';
import { Destinos } from './../models/destinos-viaje.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  destinos: Destinos[];
  constructor() { 
    this.destinos = [];
  }

  ngOnInit(): void {
  }

  guardar(nombre:string, url:string):boolean{
    this.destinos.push(new Destinos(nombre,url))
    return false;
  }
  
  elegido(d:Destinos){
    this.destinos.forEach(function(x){
      x.setSelected(false);
      d.setSelected(true);
    });
  }

}
